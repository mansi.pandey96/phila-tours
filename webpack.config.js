var path = require("path");

module.exports = {
    entry: "./app/assets/scripts/app.js",
    output:{
        path: path.resolve(__dirname,'./app/scripts'),
        filename: "app.js"
    },
    module:{
        rules: [
            {
                use:{
                    loader: 'babel-loader',
                    options:{
                        presets:['@babel/preset-env']
                    }
                },
                test: /\.js$/, /*Regular expression is written in between / / */
                exclude: /node_modules/
            }
        ]
    }
}